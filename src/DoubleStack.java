import java.util.LinkedList;
import java.util.NoSuchElementException;

public class DoubleStack {
    private LinkedList<Double> pinu;

    public static void main (String[] args) {
        String s = "1. -10. 4. 8. 3. - + * +";
        interpret(s);
        //String s = "1.  2.    +";
        //String s = "35. 10. + -";
        //String s = "35. y 10. -3. + - +";
        //String s = "35. 10. -3. + x 2.";
        //String s = "35. 10. -3. + / 2.";
    }

    DoubleStack() {
       pinu = new LinkedList<Double>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DoubleStack uus = new DoubleStack();
        uus.pinu = (LinkedList<Double>)pinu.clone();
        return uus;
    }

    public boolean stEmpty() {
        if (pinu.size() == 0) {
            //System.out.println("Pinu on tühi!");
            return true;
        }
        else {
            //System.out.println("Pinu ei ole tühi!");
            return false;
        }
    }

    public void push (double arv) {
        pinu.push(arv);
        //System.out.println("Lisasin pinule: " + arv);
    }

    public double pop() {
        if (pinu.size() == 0) throw new RuntimeException("Pinu on tühi! Ei saa popida!");
        //System.out.println("Võtsin pinust: " + pinu.peekLast());
        return pinu.pop();
    }

    public void op (String s) {
        double el2 = pop();
        double el1 = pop();
        if(s.equals("+")) push(el1 + el2);
        else if (s.equals("-")) push (el1 - el2);
        else if (s.equals("*")) push (el1 * el2);
        else if (s.equals("/")) push (el1 / el2);
        else throw new IllegalArgumentException("Sellist meil tehet ei ole!");
    }


    public double tos() {

       if (pinu.size() > 0) {
           //System.out.println("Pinu pealmine element on: " + pinu.peekFirst());
           return pinu.peekFirst();
       }
       else {
           throw new NoSuchElementException("Pinu on tühi! Pealmist elementi pole!");
       }
    }

    @Override
    public boolean equals (Object o) {
        if (((DoubleStack)o).pinu.size() == this.pinu.size() && ((DoubleStack)o).pinu.size() == 0) { // võrdleb linkedlistide suurusi - kas on tühjad?
            return true;
        }
        else if (((DoubleStack)o).pinu.size() == this.size() && ((DoubleStack)o).pinu.size() > 0) {
            for (int i = 0; i < this.size(); i++) { // kontrollib kõikide elementide võrdsust
                System.out.println("võrdlen elemente: " + ((DoubleStack)o).pinu.get(i) + "  ja: " + this.get(i));
                if (!((DoubleStack)o).pinu.get(i).equals(this.get(i))) {
                    System.out.println("ei ole võrdsed");
                    return false;
                }
            }
            System.out.println("võrdsed");
            return true;
        }
        else return false;
    }

    @Override
    public String toString() {
        if (stEmpty()) return "Tühi";
        else {
            StringBuffer puhver = new StringBuffer();
            for (int i = pinu.size(); i > 0; i--) {
                //System.out.println(pinu.get(i-1));
                puhver.append(pinu.get(i-1));
                puhver.append(" ");
            }
            return puhver.toString();
        }
    }

    public static double interpret (String pol) {
        DoubleStack magasin = new DoubleStack();
        int numberCount = 0;
        int symbolCount = 0;
        if (pol.length() == 0) throw new RuntimeException("Sõne ei tohi tühi olla!");
        String[] massiiv = pol.trim().split("\\s+|\\.\\s*");
        for (String a : massiiv) {
            //System.out.println(a);
            if ( !((isNumber(a)) || isGoodSymbol(a)))
                throw new IllegalArgumentException("Sobimatu sümbol " + a + " stringis " + pol);
            if (isNumber(a)) {
                numberCount ++;
                magasin.push(Double.parseDouble(a));
            }
            if (isGoodSymbol(a)) {
                if (magasin.size() < 2){
                    throw new IllegalArgumentException
                        ("Tehte kaks esimest märki peavad olema numbrid, mitte: " + a + ". Stringis " + pol);
                }
                magasin.op(a);
                symbolCount ++;
            }
        }
        if (numberCount - 1 != symbolCount) throw new RuntimeException(pol + " ei ole tasakaalus!");
        return magasin.tos();
    }

    public int size() {
       return pinu.size();
    }
    public Double get(int a) {
       return pinu.get(a);
    }

    public static boolean isNumber(String str) {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public static boolean isGoodSymbol(String a) {
        if (!(a.equals(String.valueOf('+')) // kontrollib kas on õige sümbol
                || a.equals(String.valueOf('-'))
                || a.equals(String.valueOf('*'))
                || a.equals(String.valueOf('/'))
        )) {
            return false;
        }
        return true;
    }
}

// viited:
// http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
// https://stackoverflow.com/questions/4767615/java-iterating-a-linked-list
// https://stackoverflow.com/questions/9650798/hash-a-double-in-java
// https://www.tutorialspoint.com/java/lang/string_split.htm
// https://stackoverflow.com/questions/1102891/how-to-check-if-a-string-is-numeric-in-java
// https://beginnersbook.com/2014/08/clone-a-generic-linkedlist-in-java/
